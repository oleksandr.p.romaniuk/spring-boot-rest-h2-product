package com.telenor.demo;

import static io.restassured.RestAssured.get;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ApplicationTest {

		/*
	- type					     |   The productEntity type. (String. Can be 'phone' or 'subscription')
- min_price				     |   The minimum price in SEK. (Number)
- max_price				     |   The maximum price in SEK. (Number)
- city					     |   The city in which a store is located. (String)
- property				     |   The name of the property. (String. Can be 'color' or 'gb_limit')
- property:color		     |	 The color of the phone. (String)
- property:gb_limit_min      |	 The minimum GB limit of the subscription. (Number)
- property:gb_limit_max      |	 The maximum GB limit of the subscription. (Number)
	 */

	// RESTful APIs
	@Test
	public void testFindAllPhones() {
		get("/product?type=phone")
		.then()
		.assertThat()
				.body("size()", is(42));
	}

	@Test
	public void testFindAllSubscriptions() {
		get("/product?type=subscription")
				.then()
				.assertThat()
				.body("size()", is(58));
	}

	@Test
	public void testFindAllByPrice() {
		get("/product?type=subscription&max_price=1000&city=Stockholm")
				.then()
				.assertThat()
				.body("size()", is(25));
	}

	@Test
	public void testFindAllByMinMaxPrice() {
		get("/product?type=subscription&min_price=700&max_price=1000&city=Stockholm")
				.then()
				.assertThat()
				.body("size()", is(9));
	}

	@Test
	public void testFindAll() {
		get("/product?type=subscription&min_price=700&max_price=1000&city=Stockholm"
				+ "&property=gb_limit&property:gb_limit_min=10&property:gb_limit_max=20")
				.then()
				.assertThat()
				.body("size()", is(4));
	}

	@Test
	public void testFindAllPhone() {
		get("/product?type=phone&min_price=700&max_price=1000&city=Laurianne"
				+ "&property:color=grå")
				.then()
				.assertThat()
				.body("size()", is(1));
	}

	@Test
	public void testFindAllParameters() {
		get("/product?type=subscription&min_price=700&max_price=1000&city=Stockholm"
				+ "&property=gb_limit&property:gb_limit_min=10&property:gb_limit_max=20&property:color=grå")
				.then()
				.assertThat()
				.body("size()", is(0));
	}

}
