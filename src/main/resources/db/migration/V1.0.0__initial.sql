CREATE TABLE product
(
   id BIGINT NOT NULL CONSTRAINT product_pkey PRIMARY KEY,
   type VARCHAR (20),
   property VARCHAR (255),
   property_value VARCHAR (255),
   price DECIMAL (19,4),
   store_address VARCHAR (255)
);

CREATE sequence IF NOT EXISTS product_id_seq;
ALTER TABLE product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq');