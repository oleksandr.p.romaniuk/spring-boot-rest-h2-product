package com.telenor.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
	String type;
	String properties;
	String price;

	@JsonProperty("store_address")
	String storeAddress;
}