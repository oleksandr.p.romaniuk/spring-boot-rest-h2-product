package com.telenor.demo.repository.specification;

import com.telenor.demo.repository.entity.ProductEntity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@AllArgsConstructor
@NoArgsConstructor
public class ProductSpecification implements Specification<ProductEntity> {

  private List<SearchCriteria> list = new ArrayList<>();

  public void add(SearchCriteria criteria) {
    list.add(criteria);
  }

  @Override
  public Predicate toPredicate(Root<ProductEntity> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    List<Predicate> predicates = new ArrayList<>();

    for (SearchCriteria criteria : list) {
      if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
        predicates.add(builder.greaterThanOrEqualTo(root.get(criteria.getKey()),
            criteria.getValue().toString()));
      } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
        predicates.add(
            builder.lessThanOrEqualTo(root.get(criteria.getKey()), criteria.getValue().toString()));
      } else if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
        predicates.add(builder.equal(
            root.get(criteria.getKey()), criteria.getValue()));
      } else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
        predicates.add(builder.like(
            builder.lower(root.get(criteria.getKey())),
            "%" + criteria.getValue().toString().toLowerCase() + "%"));
      }
    }

    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
