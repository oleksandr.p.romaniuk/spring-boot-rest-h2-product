package com.telenor.demo.repository.specification;

public enum SearchOperation {
  GREATER_THAN_EQUAL,
  LESS_THAN_EQUAL,
  EQUAL,
  MATCH
}
