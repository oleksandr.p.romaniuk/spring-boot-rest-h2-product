package com.telenor.demo.mapper;

import com.telenor.demo.model.Product;
import com.telenor.demo.repository.entity.ProductEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProductMapper {

  List<Product> mapCollection(List<ProductEntity> entity);

  @Mapping(target = "properties", expression = "java(entity.getProperty() + \":\" + entity.getPropertyValue())")
  Product map(ProductEntity entity);

}
