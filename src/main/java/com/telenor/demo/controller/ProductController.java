package com.telenor.demo.controller;

import com.telenor.demo.service.ProductService;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telenor.demo.model.Product;

@RestController
@RequestMapping("/product")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	ProductService productService;

	/*
	- type					     |   The productEntity type. (String. Can be 'phone' or 'subscription')
- min_price				     |   The minimum price in SEK. (Number)
- max_price				     |   The maximum price in SEK. (Number)
- city					     |   The city in which a store is located. (String)
- property				     |   The name of the property. (String. Can be 'color' or 'gb_limit')
- property:color		     |	 The color of the phone. (String)
- property:gb_limit_min      |	 The minimum GB limit of the subscription. (Number)
- property:gb_limit_max      |	 The maximum GB limit of the subscription. (Number)
	 */

	@GetMapping
	public ResponseEntity<?> findProduct(
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "min_price", required = false) String min_price,
			@RequestParam(value = "max_price", required = false) String max_price,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "property", required = false) String property,
			@RequestParam(value = "property:color", required = false) String propertyColor,
			@RequestParam(value = "property:gb_limit_min", required = false) String propertyMin,
			@RequestParam(value = "property:gb_limit_max", required = false) String propertyMax) {
		LOGGER.info("GET /product?type={}", type);

		try {
			List<Product> products = this.productService.findProducts(type, min_price, max_price, city,
					property, propertyColor, propertyMin, propertyMax);
			
			return new ResponseEntity<>(products, HttpStatus.OK);
			
		} catch (Exception e) {
			LOGGER.error("GET /product?type={} - ERROR: {}", type, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}