package com.telenor.demo.service;

import com.telenor.demo.mapper.ProductMapper;
import com.telenor.demo.model.Product;
import com.telenor.demo.repository.ProductRepository;
import com.telenor.demo.repository.specification.ProductSpecification;
import com.telenor.demo.repository.specification.SearchCriteria;
import com.telenor.demo.repository.specification.SearchOperation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProductService {

  ProductMapper productMapper;
  ProductRepository productRepository;

  public List<Product> findProducts(String type, String min_price, String max_price, String city,
      String property, String propertyColor, String propertyMin, String propertyMax) {

    ProductSpecification productSpecification = new ProductSpecification();

    if (!Strings.isEmpty(type)) {
      productSpecification.add(new SearchCriteria("type", type, SearchOperation.EQUAL));
    }
    if (!Strings.isEmpty(min_price)) {
      productSpecification.add(
          new SearchCriteria("price", min_price, SearchOperation.GREATER_THAN_EQUAL));
    }
    if (!Strings.isEmpty(max_price)) {
      productSpecification.add(
          new SearchCriteria("price", max_price, SearchOperation.LESS_THAN_EQUAL));
    }
    if (!Strings.isEmpty(city)) {
      productSpecification.add(new SearchCriteria("storeAddress", city, SearchOperation.MATCH));
    }
    if (!Strings.isEmpty(property)) {
      productSpecification.add(new SearchCriteria("property", property, SearchOperation.EQUAL));
    }
    if (!Strings.isEmpty(propertyColor)) {
      productSpecification.add(
          new SearchCriteria("propertyValue", propertyColor, SearchOperation.MATCH));
    }
    if (!Strings.isEmpty(propertyMin)) {
      productSpecification.add(
          new SearchCriteria("propertyValue", propertyMin, SearchOperation.GREATER_THAN_EQUAL));
    }
    if (!Strings.isEmpty(propertyMax)) {
      productSpecification.add(
          new SearchCriteria("propertyValue", propertyMax, SearchOperation.LESS_THAN_EQUAL));
    }
    return productMapper.mapCollection(productRepository.findAll(productSpecification));
  }
}
